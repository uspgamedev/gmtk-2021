extends PathFollow2D

class_name Enemy

signal died(enemy)

var hp : int
var speed : int

func _physics_process(delta):
	if get_unit_offset() < 1:
		set_offset(get_offset() + speed*delta)

func take_damage(dmg):
	hp -= dmg
	print('ai ai porra agora tenho ', hp, ' de vida')
	if hp <= 0:
		die()

func die():
	Audio.SFX(2)
	print('aqui terminam minhas desventuras')
	self.emit_signal("died", self)
	call_deferred("free")
