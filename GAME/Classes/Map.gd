class_name Map
extends Node2D
# It's necessary the GUI scene, a Path2D, a Timer named EnemySpawnTimer and a Node2D named Towers

export(PackedScene) var common_enemy
export(PackedScene) var fading_enemy
export(PackedScene) var mage_enemy
export(PackedScene) var spell_scene

var fade_scene : PackedScene = preload("res://Entities/Fade/Fade.tscn")
var next_scene : String

var enemies    : Array = []
var num_common : int
var num_fading : int
var num_mage   : int
var enem_total : int

func _ready():
	$GUI.connect("start_wave", self, "on_gui_start_wave")
	$GUI.connect("restart", self, "on_gui_restart")
	var fade = fade_scene.instance()
	add_child(fade)
	fade.fade_in()

func _unhandled_input(event):
	if event.is_action_pressed("action_lasso"):
		for tower in $Towers.get_children():
			tower.be_lassoed($MainGuy)

func on_gui_start_wave():
	$EnemySpawnTimer.connect("timeout", self, "enemy_spawn")
	$EnemySpawnTimer.start()

func enemy_spawn():
	pass

func on_gui_restart():
	next_scene = self.filename
	var fade = fade_scene.instance()
	add_child(fade)
	fade.connect("end", self, "on_fade_out_end")
	fade.fade_out()

func on_fade_out_end():
	get_tree().change_scene(next_scene)

func launch_spell(spawn_pos : Vector2, dest : Vector2):
	var spell_instance = spell_scene.instance()
	self.add_child(spell_instance)
	
	# Get last spell
	var last_spell = self.get_child(self.get_child_count() - 1)
	last_spell.set_target(spawn_pos, dest)
