extends Node

var Electric: = load("res://Assets/Assets/SFX/qubodupElectricityDamage02.wav")
var Death: = load("res://Assets/Assets/SFX/scream_horror1.wav")
var Monster: = load("res://Assets/Assets/SFX/Large Monster Death 01.wav")
var _BGM: = load("res://Assets/Assets/SFX/MyVeryOwnDeadShip.ogg")
var Menu: = load("res://Assets/Assets/SFX/Menu Selection Click.wav")
var audio: = [Electric, Death, Monster, _BGM, Menu,]
var music = null
var sound_effect = null
onready var pause_music : bool = false

func SFX(id,volume = -8):
	if pause_music != true:
		if sound_effect == null or get_children().size() < 2:
			sound_effect = AudioStreamPlayer.new()
			self.add_child(sound_effect)
			sound_effect.stream = audio[id]
			sound_effect.volume_db = volume
			sound_effect.play(0)
		elif sound_effect.playing == true and sound_effect.stream == audio[id]:
			return
		else:
			sound_effect = AudioStreamPlayer.new()
			self.add_child(sound_effect)
			sound_effect.stream = audio[id]
			sound_effect.volume_db = volume
			sound_effect.play(0)

func BGM(id):
	if pause_music != true:
		music = AudioStreamPlayer.new()
		self.add_child(music)
		music.stream = audio[id]
		music.volume_db = -2
		music.play(0)

func stop_music():
	if pause_music != true:
		pause_music = true
		for node in get_children():
			node.call_deferred("free")
	else:
		pause_music = false
		BGM(3)
