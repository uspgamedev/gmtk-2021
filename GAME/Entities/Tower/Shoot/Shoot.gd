extends Line2D

const POINT_VARIANCE := 20
const POINT_SHAKE := 20

var target : Node2D

signal damage(amount)

func _ready():
	if target == null:
		print('nao se pode matar aquilo que nao se pode ver')
		self.call_deferred("free")
		return
	connect("damage", target.get_parent(), "take_damage")
	self.points[3] = (target.global_position - self.global_position)

	self.points[1] = self.points[3]/3
	self.points[2] = 2*self.points[3]/3

	self.points[1] += Vector2(rand_range(-POINT_VARIANCE, POINT_VARIANCE), rand_range(-POINT_VARIANCE, POINT_VARIANCE))
	self.points[2] += Vector2(rand_range(-POINT_VARIANCE, POINT_VARIANCE), rand_range(-POINT_VARIANCE, POINT_VARIANCE))

func _process(_delta):
	if target != null:
		self.points[3] = (target.global_position - self.global_position)
		self.points[1] += Vector2(rand_range(-POINT_SHAKE, POINT_SHAKE), rand_range(-POINT_SHAKE, POINT_SHAKE))
		self.points[2] += Vector2(rand_range(-POINT_SHAKE, POINT_SHAKE), rand_range(-POINT_SHAKE, POINT_SHAKE))

func _on_animation_finished(_anim_name):
	self.call_deferred("free")
	emit_signal('damage', 1)
