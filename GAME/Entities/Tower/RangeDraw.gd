extends Node2D

func _draw():
	draw_arc(Vector2.ZERO, $"../ShootArea/CollisionShape2D".shape.get_radius(), 0, 360, 1350, Color.purple, 1)
	draw_arc(Vector2.ZERO, $"../RejectionArea/CollisionShape2D".shape.get_radius(), 0, 360, 1350, Color.red, 1)

func _unhandled_input(event):
	if event.is_action_pressed("toggle_range"):
		self.visible = !self.visible
