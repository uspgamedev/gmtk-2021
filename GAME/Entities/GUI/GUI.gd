extends Node2D

export(int) var countdown_wait_time
signal start_wave
signal restart

var Mute = load("res://Assets/Assets/Buttons/MuteBlue.png")
var Play = load("res://Assets/Assets/Buttons/VolumeBlue.png")

func _ready():
	# warning-ignore:return_value_discarded
	$PlayBtn.connect("pressed", self, "on_play_pressed")
	# warning-ignore:return_value_discarded
	$RestartBtn.connect("pressed", self, "on_restart_pressed")
	# warning-ignore:return_value_discarded
	$Countdown/Timer.connect("timeout", self, "on_countdown_timeout")
	$Countdown/Timer.wait_time = countdown_wait_time
	$Countdown/Timer.start()

	if Audio.pause_music:
		get_node("MusicBtn/Sprite").texture = Mute

func on_play_pressed():
	Audio.SFX(4,0)
	$Countdown/Timer.stop()
	$PlayBtn.disabled = true
	$PlayBtn/Sprite.modulate.a = 0.5
	$Countdown.modulate.a = 0.5
	emit_signal("start_wave")

func on_restart_pressed():
	Audio.SFX(4,0)
	emit_signal("restart")

func on_countdown_timeout():
	$PlayBtn.disabled = true
	$PlayBtn/Sprite.modulate.a = 0.5
	$Countdown.modulate.a = 0.5
	emit_signal("start_wave")


func _on_MusicBtn_button_down():
	Audio.SFX(4,0)
	var sprite = get_node("MusicBtn/Sprite")
	Audio.stop_music()
	if sprite.texture == Play:
		sprite.texture = Mute
	else:
		sprite.texture = Play
