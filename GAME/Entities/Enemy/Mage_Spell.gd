extends Node2D

var target : Vector2

func set_target(spawn_pos : Vector2, dest : Vector2):
	target = dest
	self.position = spawn_pos
	self.rotation_degrees = get_angle_to(target)*(180/PI)

func _physics_process(delta):
	self.global_position = self.global_position.linear_interpolate(target, 2*delta)

func _on_Area2D_body_entered(body):
	if(body.is_in_group("tower")):
		body.take_damage(1)
		queue_free()

func _on_Timer_timeout():
	queue_free()
