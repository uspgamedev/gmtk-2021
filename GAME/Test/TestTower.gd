extends Node2D

onready var Tower = $Tower
onready var Tower2 = $Tower2
onready var Tower3 = $Tower3
onready var MainGuy = $MainGuy

func _unhandled_input(event):
	if event.is_action_pressed("action_lasso"):
		Tower.be_lassoed(MainGuy)
		Tower2.be_lassoed(MainGuy)
		Tower3.be_lassoed(MainGuy)
