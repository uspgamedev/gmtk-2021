extends Node2D

export(Array, PackedScene) var map_list := []

var curr_map : PackedScene

func _ready():
	assert(not map_list.empty(), "MAP LIST IS EMPTY")
	curr_map = map_list.pop_front()
	load_map()

func next_map():
	if map_list.empty():
		print("CONGRATZ GAME IS OVER NOW RATE 5 ON ICHIOUOU")
		get_tree().change_scene("res://Entities/Misc/MainMenu.tscn")
		return

	self.get_child(1).call_deferred("free")
	curr_map = map_list.pop_front()
	load_map()

func restart_map():
	self.get_child(1).call_deferred("free")
	load_map()

func load_map():
	var map = curr_map.instance()
	map.connect("map_won", self, "next_map")
	map.connect("map_restart", self, "restart_map")
	map.modulate = Color(0, 0, 0, 0)
	self.call_deferred("add_child", map)
	$Tween.remove_all()
	$Tween.interpolate_property(map, "modulate", map.modulate, Color.white, 0.3, Tween.TRANS_CIRC)
	$Tween.start()
